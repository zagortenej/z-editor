'use strict'

define(['module', 'jquery',
    'zeditor/ZUtil', 'zeditor/ZConfig', 'zeditor/ZEditor'
    ], function(module, $j, ZUtil, ZConfig, ZEditor){

    var self = this
    var ZBuilder = {}
    self.settings =
    {
    }

    var initialize = function( config )
    {
        $j.extend(true, self.settings, config || {})

        // if(ZUtil.isInsideElectron())
        // {
        //     ZBuilderElectron.extend(ZBuilder, self.settings)
        // }
        // else
        // {
        //     ZBuilderWeb.extend(ZBuilder, self.settings)
        // }
    }

    var demo = function(term, size)
    {
        function progress(percent, width) {
            var size = Math.round(width*percent/100);
            var left = '', taken = '', i;
            for (i=size; i--;) {
                taken += '=';
            }
            if (taken.length > 0) {
                taken = taken.replace(/=$/, '>');
            }
            for (i=width-size; i--;) {
                left += ' ';
            }
            return '[' + taken + left + '] ' + percent + '%';
        }
        var animation = false;
        var timer;
        var prompt;
        var string;
        if (true) {
            var i = 0;
            prompt = term.get_prompt();
            string = progress(0, size);
            term.set_prompt(progress);
            animation = true;
            (function loop() {
                string = progress(i++, size);
                term.set_prompt(string);
                if (i < 100) {
                    timer = setTimeout(loop, 50);
                } else {
                    term.echo(progress(i, size) + ' [[b;green;]OK]')
                        .set_prompt(prompt);
                    term.echo('done.')
                    animation = false
                }
            })();
        }
    }

    var buildStart = function()
    {
        // Initiates the build on server side
    }

    var buildProgress = function()
    {
        // Periodically prints out build output in the terminal
    }

    var build = function()
    {
        var dfd = $j.Deferred()

        ZConfig.loadConfig('config/build')
        .done(function(config){
            
            var terminalOutput = ZEditor.terminalOutput()
            terminalOutput.clear()
            terminalOutput.echo('building...')
            demo(terminalOutput, 80)

            dfd.resolve()
        })

        return dfd.promise()
    }

    // public functions interface
    $j.extend(true, ZBuilder, {
        build: build,
    })
    initialize(module.config())

    return ZBuilder

})
