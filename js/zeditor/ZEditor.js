'use strict'
/*
    Requires:
        - jQuery
        - Bootstrap JavaScript
        - ZUtil
        - ZService
        - ace.js (https://ace.c9.io)
        - jquery.terminal.js (https://github.com/jcubic/jquery.terminal)
*/

define(['module', 'jquery',
    'zeditor/ZUtil', 'zeditor/ZConfig', 'zeditor/ZService', 'text!fragments/tabPanelFragments.html',
    'terminal', 'ace', 'bootstrap',
    'domReady!'], function(module, $j, ZUtil, ZConfig, ZService, tabPanelFragments){

    var ZEditor = {}
    var settings =
    {
        tabPanelFragments: tabPanelFragments,
        tabNavTemplate: '',
        tabPanelTemplate: '',

        tabNavsContainer: '',
        tabPanelsContainer: '',

        projectFolder: '',

        terminalPanel: '',
        terminalGreeting: 'Z-Editor Output',
    }

    // private variables
    var $tabNavsContainer = null
    var $tabPanelsContainer = null

    var terminal = null

    // to keep track of open files (tabs)
    // elements are objects returned by tabPanelCreate() function
    var tabs = []

    // shorthand
    var formatString = ZUtil.formatString

    // All functions called by initialize() have to return Promise
    var initialize = function( config )
    {
        $j.extend(true, settings, config || {})

        sanitizeRequired()

        initTabTemplates()
        initVars()
    }

    var initTabTemplates = function()
    {
        var fragments = ZUtil.parseFragments(settings.tabPanelFragments)

        settings.tabNavTemplate = fragments.tabNav
        settings.tabPanelTemplate = fragments.tabPanel
    }

    var sanitizeRequired = function()
    {
        settings.projectFolder = ZUtil.ensureLeadingSlash(ZUtil.ensureTrailingSlash(settings.projectFolder))
    }

    var initVars = function()
    {
        $tabNavsContainer = $j(settings.tabNavsContainer)
        $tabPanelsContainer = $j(settings.tabPanelsContainer)
    }

    var stringToId = function(text)
    {
        return text.replace(/[^a-zA-Z0-9]/g, '_') + '_' + String((Math.random() * 1000000).toFixed(0))
    }

    var tabPanelCreate = function( id, title, content, closeCallback = ZUtil.noop )
    {
        id = String(id)
        title = String(title)
        content = String(content)
        
        var $tab = $j(formatString(settings.tabNavTemplate, {tabId: id, tabTitle: title}))
        var $panel = $j(formatString(settings.tabPanelTemplate, {tabId: id, panelContent: content}))

        $tabNavsContainer.append($tab)
        $tabPanelsContainer.append($panel)
        
        // handler for tab close event
        $tab.find('.close-tab').click( function(e){
            closeCallback(id)
        })

        // make the whole tab a tab, eh?
        $tab.tab()

        // register tab with our 'registry'
        tabs[id] =
        {
            $tab: $tab,
            $panel: $panel
        }

        return tabs[id]
    }

    var tabPanelDestroy = function( id )
    {
        tabs[id].$tab.find('.close-tab').off('click')
        tabs[id].$tab.remove()
        tabs[id].$panel.remove()

        // remove entry from tabs[] array
        delete tabs[id]
        // TODO: if we end up with empty tabs[], create blank tab

        // TODO: focus on the previous focused?
        // make the first tab from registry active
        for (var tabId in tabs)
        {
            tabs[tabId].$tab.find('a').click()
            break;
        }
    }

    var tabPanelFocus = function( id )
    {
        // this brings the tab into focus correctly
        tabs[id].$tab.find('a').click()
    }

    var createAceEditor = function(element, config, text)
    {
        // var Document = ace.require("ace/document").Document
        // FIXME
        if(!(element && text !== undefined))
        {
            console.error('initializeEditor: undefined parameters')
            return
        }

        var editor = ace.edit(element)
        editor.setTheme("ace/theme/" + (config.theme || 'monokai'))
        editor.setFontSize(config.fontSize || '12px')
        var newSession = ace.createEditSession(text)
        newSession.setMode("ace/mode/c_cpp")
        editor.setSession(newSession)

        return editor
    }

    var destroyAceEditor = function(editor)
    {
        editor.destroy();
    }

    // handles click on tab's 'X' button
    var onCloseFile = function(id)
    {
        // esentially a tab that was clicked on
        var $li = $j('[data-tab-id="'+id+'"]')
        var zdata = $li.data('zeditor')

        // TODO: check if editor is dirty -> ask for save
        console.log('zdata.aceEditor.getSession', zdata.aceEditor.getSession())
        // destroy ace
        destroyAceEditor(zdata.aceEditor)
        // destroy DOMs/data()
        tabPanelDestroy(zdata.id)
    }

    var openFile = function( fileName )
    {
        var dfd = $j.Deferred()

        var fileLocation = settings.projectFolder + ZUtil.removeLeadingSlash(fileName)

        ZConfig.loadConfig('config/ide')
        .done(function(config){
            ZService.loadFile(fileLocation, false)
            .done(function(fileContent){
                // This is tab panel DOM element id used by Bootstrap tab() and by ace editor
                var id = stringToId(fileName)
                
                // create a tab with empty content
                // ace editor will load the content and set it up properly
                var tabPanel = tabPanelCreate(id, fileName, '', onCloseFile)

                var editor = createAceEditor(id, config.editor, fileContent)

                // attach some data to the tab itself for us to use later
                tabPanel.$tab.data('zeditor', {
                    id: id,
                    fileName: fileName,
                    aceEditor: editor,
                    aceDocument: editor.getSession().getDocument(),
                })

                tabPanelFocus(id)

                dfd.resolve(id)
            })
            .fail(function(){
                // TODO failed to open file
                dfd.reject()
            })
        })

        return dfd.promise()
    }

    var saveFile = function( fileName, content )
    {
        var dfd = $j.Deferred()

        var fileLocation = settings.projectFolder + ZUtil.removeLeadingSlash(fileName)

        ZService.saveFile(fileLocation, content, false)
        .done(function(response){
            // TODO do some other things?
            dfd.resolve()
        })
        .fail(function(){
            // TODO failed to save file
            dfd.reject()
        })

        return dfd.promise()
    }

    var createContentTab = function(title, content)
    {
        var id = stringToId(title)
        tabPanelCreate(id, title, content, closeContentTab)
        tabPanelFocus(id)
        return id
    }

    var closeContentTab = function(id)
    {
        tabPanelDestroy(id)
    }

    var initTerminalOutput = function()
    {
        var terminal = $j(settings.terminalPanel).terminal(function(command, term){
            // Nothing happens here, we use terminal just for output
        }, {
            greetings: settings.terminalGreeting,
            name: stringToId(settings.terminalPanel),
            prompt: '',
        })

        // disable user input and stuff
        terminal.freeze(true)

        return terminal
    }

    var getTerminalOutput = function()
    {
        if(terminal !== null)
        {
            return terminal
        }

        return initTerminalOutput()
    }

    initialize(module.config())
    // public functions interface
    $j.extend(true, ZEditor, {
        openFile: openFile,
        saveFile: saveFile,
        newTab: createContentTab,
        closeTab: closeContentTab,
        terminalOutput: getTerminalOutput,
    })

    return ZEditor

})
