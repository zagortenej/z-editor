// Electron (http://electron.atom.io/) has its own 'require' function
// so we need to use 'requirejs' in order for RequireJS and Electron
// to work together

if(typeof require !== "undefined")
{
    // this code is a hack
    // If we are running in Electron, this will save Electron's require() for us
    // to use in parts of the app that run only when Electron is available
    // If we are running in web browser, requirenode will actually be requirejs,
    // but that is not important since when we run in web browser we never call
    // reaquirenode().
    // Important thing here is that we are deleting require, exports
    // and module so jQuery and other libraries will load
    // correctly with RequireJS in Electron.
    // If you want to see error messages, comment these lines out, open dev tools console
    // and load the app in Electron.
    window.requirenode = require;
    delete window.require;
    delete window.exports;
    delete window.module;
}

requirejs.config({
    // entry point
    // deps: ["zeditor/main"],

    baseUrl: "js/lib",
    paths:
    {
        zeditor: '../zeditor',
        fragments: '../../assets/fragments',

        jquery: 'jquery-3.1.1',
        jquerysse: 'jquery.sse.min',
        bootstrap: 'bootstrap.min',
        ace: 'ace/ace',
        velocity: 'velocity.min',
        velocityui: 'velocity.ui.min',
        terminal: 'jquery.terminal-0.11.23.min',
    },

    // urlArgs: "_k=" + (new Date()).getTime()

    // http://requirejs.org/docs/api.html#config-shim
    shim:
    {
        // jquery defines 'jquery' when it detects requirejs/AMD loader
        'bootstrap': ['jquery'],
        'jquerysse': ['jquery'],
        'terminal': ['jquery'],
        'velocity': ['jquery'],
        'velocityui': ['velocity'],
        'ace':
        {
            // exports: 'ace',
        },
    },

    config:
    {
        'zeditor/ZService':
        {
            filesLocation: 'assets',
            // apiUrl: '/htbin/files.py',
            apiUrl: '/files',
        },
        'zeditor/ZUtil':
        {
        },
        'zeditor/ZEditor':
        {
            projectFolder: '/projects',

            tabNavsContainer: '#navTabs',
            tabPanelsContainer: '#tabPanes',

            terminalPanel: '.terminal',
        },
    },
})

// http://stackoverflow.com/a/37480521
// Electron specific: insert this line above script imports
// if (typeof module === 'object') {window.module = module; module = undefined;}

requirejs(['zeditor/main'], function (){
})

// Electron specific: insert this line after script imports
// if (window.module) module = window.module;
