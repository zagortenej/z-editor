#!/usr/bin/env node

// kws: nodejs Error: spawn ENOENT
// http://stackoverflow.com/a/27883443
(function() {
    var childProcess = require("child_process");
    var oldSpawn = childProcess.spawn;
    function mySpawn() {
        console.log('spawn called');
        console.log(arguments);
        var result = oldSpawn.apply(this, arguments);
        return result;
    }
    childProcess.spawn = mySpawn;
})();


console.log('hello!!')

// http://stackoverflow.com/a/17466459
function run_cmd(cmd, args, callBack )
{
    var spawn = require('child_process').spawn
    var child = spawn(cmd, args
        // ,{cwd: 'c:\\\\Users\\nprasovic\\Documents\\misc\\zeddy\\test'}
        )
    var resp = ""
    var me = this

    // https://nodejs.org/api/child_process.html#child_process_child_process_spawn_command_args_options
    child.stdout.on('data', function (data) { resp += data.toString(); console.log(data.toString()) })
    child.stderr.on('data', function (data) { resp += data.toString(); console.log(data.toString()) })
    // child.stdout.on('end', function() {  })
    // child.stderr.on('end', function() {  })
    child.on('error', function(err) { console.log('failed to start child process', err) })
    // 'close' is called when stdio streams have closed
    // child.on('close', function(code) { me.exit_code = code; callBack (resp) })
    child.on('exit', function(code) { console.log("we're done here: ", code); me.exit_code = code; callBack (resp) })

    // process.stdin.resume()
    // process.stdin.on('data', function (chunk) {
    //     child.stdin.write(chunk)
    // })
}

// run_cmd( "ls", ["-l"], function(text) { console.log (text) });

// run_cmd( "python", ["longrun.py"], function(text) { console.log ('callBack') });

// run_cmd("ConsoleApplication1.exe", [], function(text) { /*console.log (text)*/ });
// run_cmd("c:\\\\Users\\nprasovic\\Documents\\misc\\zeddy\\test\\ConsoleApplication1.exe", [], function(text) { console.log (text) });


run_cmd( "gcc", ["--version"], function(text) { console.log ('callBack') });
