#!/usr/bin/python

import cherrypy

import os

import time


class ZEditorBackend(object):
    @cherrypy.expose
    def index(self):
        return """
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
</head>
<body>
  <script>
    //var source = new EventSource('/simple');
    var source = new EventSource('/thing');
    source.onmessage = function(e) {
      document.body.innerHTML += e.data + '<br>';
      console.log('e', e)
      if(e.lastEventId == 'CLOSE'){
        console.log('close event')
        source.close();
    }
    };
  </script>
</body>
</html>
        """

    @cherrypy.expose
    def simple(self):
        cherrypy.response.headers['Content-Type'] = 'text/event-stream'
        cherrypy.response.headers['Cache-Control'] = 'no-cache'

        result = "id: 1\ndata: Hello, \n\n"
        result += "id: 2\ndata: world \n\n"
        result += "id: CLOSE\ndata: dude!\n\n"
        return result

    @cherrypy.expose
    def thing(self):
        cherrypy.response.headers['Content-Type'] = 'text/event-stream'
        cherrypy.response.headers['Cache-Control'] = 'no-cache'
        def content():
            yield "id: 1\ndata: Hello, \n"
            # time.sleep(1)
            yield "id: 2\ndata: world \n"
            # time.sleep(1)
            yield "id: CLOSE\ndata: dude!\n"
        return content()
    thing._cp_config = {'response.stream': True}


cherrypy.config.update({'server.socket_port': 9000})
cherrypy.tree.mount(ZEditorBackend())

cherrypy.engine.start()
cherrypy.engine.block()
