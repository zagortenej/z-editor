#!/usr/bin/env python

import cgi
import json


form = cgi.FieldStorage()
 
val1 = form.getvalue('first')
val2 = form.getvalue('last')


print "Content-type: application/json"
print
print json.dumps(
    [
        'foo',
        {
            'bar': ('baz', None, 1.0, 2)
        },
        {
            'name': val1,
            'given': val2
        }
    ]
)
