#!/usr/bin/python

import sys
import re

from arpeggio import visit_parse_tree
from arpeggio.cleanpeg import ParserPEG, PTNodeVisitor

zconf_peg = """
zconf = ( expression / newline )* EOF

expression = comment / section / keyval

comment = comment_start comment_content
comment_content = val_line
comment_start = r'[#;]'

section = '[' section_name ']' ws comment?
section_name = key_name

keyval = key ws keyval_sep ws val
keyval_sep = r'[=:]'
key = key_name
key_name = r'[a-zA-Z0-9._-]+'

val = val_multiline / val_line
val_multiline = newline val_m_line*
val_m_line = (hws val_line / comment)? newline
val_line = r'([^\n])*'

ws = ('\t' / ' ')*
hws = ('\t' / ' ')+

newline = '\n' / '\r\n'
"""

class ZConfVisitor(PTNodeVisitor):
    def __init__(self, debug=False):
        super(ZConfVisitor, self).__init__(self)
        if not self.debug:
            self.debug = debug
        self.zconf = {}
        self.section = 'DEFAULT'
        self.zconf[self.section] = {}

    def visit_zconf(self, node, children):
        if self.debug:
            print "---->>>> zconf -> '%s'" % (children)

        return self.zconf

    def visit_comment(self, node, children):
        if self.debug:
            print "---->>>> comment -> '%s' '%s'" % (node.value, children)

        return None

    def visit_section(self, node, children):
        if self.debug:
            print "---->>>> section -> '%s' '%s'" % (node.value, children)

        self.section = unicode(children[0])
        self.zconf[self.section] = {}
        return self.section

    def visit_keyval(self, node, children):
        if self.debug:
            print "---->>>> keyval -> '%s' '%s' '%s'" % (node.value, children[0], children[-1])

        self.zconf[self.section][children[0]] = unicode(children[-1])
        return (children[0], unicode(children[-1]))

    def visit_key_name(self, node, children):
        if self.debug:
            print "---->>>> key_name -> '%s' '%s'" % (unicode(node.value), children)

        return unicode(node.value)

    def visit_val_multiline(self, node, children):
        if self.debug:
            print "---->>>> val_multiline -> '%s' '%s'" % (unicode(node.value), children)

        return u''.join(children)

    def visit_val_m_line(self, node, children):
        if self.debug:
            print "---->>>> val_m_line -> '%s' '%s'" % (unicode(node.value), children)

        return u''.join(children)

    def visit_val_line(self, node, children):
        if self.debug:
            print "---->>>> val_line -> '%s' '%s'" % (unicode(node.value), children)

        return unicode(node.value)

    def visit_ws(self, node, children):
        if self.debug:
            print "---->>>> ws -> '%s' '%s'" % (node.value, children)

        return u''.join(children)

    def visit_hws(self, node, children):
        if self.debug:
            print "---->>>> hws -> '%s' '%s'" % (node.value, children)

        return u''.join(children)

    def visit_newline(self, node, children):

        return u'\n'

class ZConfParser:
    def __init__(self, zconffile=None, zconfgrammarfile=None):
        self.grammar(zconfgrammarfile)
        self.zconf = {}
        if zconffile:
            self.read(zconffile)

    def grammar(self, zconf_grammar_file):
        zconf_grammar = ''

        if not zconf_grammar_file:
            zconf_grammar = zconf_peg
        else:
            with open(zconf_grammar_file) as file:
                zconf_grammar = file.read()

        # self.parser = ParserPEG(zconf_grammar, "zconf", skipws=False, debug=True, reduce_tree=True)
        self.parser = ParserPEG(zconf_grammar, "zconf", skipws=False)

    def read(self, zconf_file):
        with open(zconf_file) as file:
            zconf_content = file.read()
        parse_tree = self.parser.parse(zconf_content)

        # self.zconf = visit_parse_tree(parse_tree, ZConfVisitor(debug=True))
        self.zconf = visit_parse_tree(parse_tree, ZConfVisitor())

    def config(self):
        return self.zconf

    def get(self, section, option):
        return self.zconf[section][option]

    def items(self, section):
        return self.zconf[section].items()

    def options(self, section):
        return self.zconf[section].keys()

    def sections(self):
        return self.zconf.keys()


if __name__ == '__main__':
    import pprint
    pp = pprint.PrettyPrinter(indent=4)

    # config = ZConfParser(zconffile='test.zconf', zconfgrammarfile='zconf.peg')
    config = ZConfParser(zconffile='test.zconf')
    # config.read(zconffile='test.zconf')

    pp.pprint(config.config())

    pp.pprint(config.sections())

    for section in config.sections():
        print 'section: ', section
        pp.pprint(config.items(section))

    print "config.get('sectio_n3', 'key3') = '{}'".format(config.get('sectio_n3', 'key3'))

