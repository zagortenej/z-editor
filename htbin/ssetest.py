#!/usr/bin/python

import json
import requests

r = requests.get('http://127.0.0.1:8000/thing', stream=True)

for line in r.iter_lines():

    # filter out keep-alive new lines
    if line:
        decoded_line = line.decode('utf-8')
        # print(json.loads(decoded_line))
        print(decoded_line)

