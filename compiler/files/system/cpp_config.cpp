// http://pabigot.github.io/bspacm/newlib.html

#include <stdlib.h> // malloc, free


void *operator new(size_t size)
{
    return malloc(size);
}

void *operator new[](size_t size)
{
    return malloc(size);
}

void operator delete(void *p)
{
    free(p);
}

void operator delete[](void *p)
{
    free(p);
}


extern "C"
{

#ifdef CPP_NO_HEAP
void *malloc(size_t)
{
    return (void *)0;
}

void free(void *)
{
}
#endif

// https://sourceware.org/git/gitweb.cgi?p=newlib-cygwin.git;a=blob;f=newlib/libc/sys/arm/aeabi_atexit.c
// library implementation pulls in __cxa_atexit and other stuff.
// we don't need it
int __aeabi_atexit(void *arg, void (*func) (void *), void *d)
{
    return 0;
}

} // extern "C"


/*
    since we don't use RTTI, exceptions and similar stuff in our embedded environment
    we'll provide dummy implementations of some library functions which will prevent
    pulling in a large amounts of code we don't need.
*/

extern "C" void __cxa_pure_virtual()
{
    // serial.print("pure virtual function called. halting.")
    while (1);
}

namespace __gnu_cxx
{
    void __verbose_terminate_handler()
    {
        // serial.print("unhandled exception. halting.")
        while (1);
    }
}
