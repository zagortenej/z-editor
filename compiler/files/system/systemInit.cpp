/*
  variant.cpp - implementation of initVariant() and other platform-specific Arduino functions
  Copyright (c) 2016 Ravendyne Inc.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

// #include "board/board_variant.h"
// #include "board_private.h"
// #include "board/board_serial.h"

#include "chip.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * All LPC IRQ handlers and such have to be implemented
 * within extern "C" declaration, otherwise their names will get mangled
 * by C++ compiler and they WON'T get linked where you would expect them
 * to be because they are declared with __attribute__((weak)).
 */

/**
 * @brief   Handle interrupt from SysTick timer
 * @return  Nothing
 */
void SysTick_Handler(void)
{
    //Board_LED_Toggle(0);
}

void yield(void)
{
    __WFI();
}

/* System oscillator rate and clock rate on the CLKIN pin */
/*
  Needed by Chip_Clock_GetMainOscRate() in clock_13xx.h
  and ultimately used by Chip_Clock_GetMainClockRate() (and others)
  which is then called by other functions like SPI, UART, I2C etc.
*/
const uint32_t OscRateIn = 12000000;
/* Not used but needs to be declared */
const uint32_t ExtRateIn = 0;


/* Pin muxing table, only items that need changing from their default pin
   state are in this table. */
STATIC const PINMUX_GRP_T pinmuxing[] = {
  {0,  1,  (IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT)},  /* PIO0_1 used for CLKOUT */
  {0,  2,  (IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP)}, /* PIO0_2 used for SSEL */
  {0,  3,  (IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT)},  /* PIO0_3 used for USB_VBUS */
  {0,  4,  (IOCON_FUNC1 | IOCON_FASTI2C_EN)},             /* PIO0_4 used for SCL */
  {0,  5,  (IOCON_FUNC1 | IOCON_FASTI2C_EN)},             /* PIO0_5 used for SDA */
  {0,  6,  (IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT)},  /* PIO0_6 used for USB_CONNECT */
  {0,  8,  (IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT)},  /* PIO0_8 used for MISO0 */
  {0,  9,  (IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT)},  /* PIO0_9 used for MOSI0 */
  {0,  11, (IOCON_FUNC2 | IOCON_ADMODE_EN    | IOCON_FILT_DIS)},  /* PIO0_11 used for AD0 */
  {0,  18, (IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT)},  /* PIO0_18 used for RXD */
  {0,  19, (IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT)},  /* PIO0_19 used for TXD */
  {1,  29, (IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT)},  /* PIO1_29 used for SCK0 */
};


void Board_SystemInit(void)
{
  /* Booting from FLASH, so remap vector table to FLASH */
  Chip_SYSCTL_Map(REMAP_USER_FLASH_MODE);

  Chip_SetupXtalClocking();

  /* Enable IOCON clock */
  Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_IOCON);

  /* TODO: pin muxing should go to board variant support files */
  Chip_IOCON_SetPinMuxing(LPC_IOCON, pinmuxing, sizeof(pinmuxing) / sizeof(PINMUX_GRP_T));
}


void lowLevelSystemInit( void )
{
  // Board_SystemInit();

  /*
    initialize peripherals that are expected
    to be present by Zeeduino core:
    serial, adc, pwm, delay etc.
    i.e. these are supposed to exist for any board variant
  */

  // Set systick timer to 1ms interval
  SysTick_Config(SystemCoreClock / 1000);

  // Board_Delay_InitTimer();
  // Board_ADC_Init();
  // Board_PWM_Init_CT32B0();
  // Board_PWM_Init_CT16B0();
  // Board_PWM_Init_CT16B1();
  // Serial_Init();
}


#ifdef __cplusplus
}
#endif
