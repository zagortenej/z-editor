#include <stdio.h>

volatile unsigned int * const UART0DR = (unsigned int *)0x4000c000;
 
void print_uart0(const char *s) {
 while(*s != '\0') { /* Loop until end of string */
 *UART0DR = (unsigned int)(*s); /* Transmit char */
 s++; /* Next char */
 }
}

static char hex [] = { '0', '1', '2', '3', '4', '5', '6', '7',
                        '8', '9' ,'A', 'B', 'C', 'D', 'E', 'F' };
 
const char* toHex(int number)
{
    static char buffer[32];

    buffer[0] = 0;

    int len=0,k=0;
    do//for every 4 bits
    {
        //get the equivalent hex digit
        buffer[len] = hex[number & 0xF];
        len++;
        number>>=4;
    }while(number!=0);
    //since we get the digits in the wrong order reverse the digits in the buffer
    for(; k<len/2; k++)
    {//xor swapping
        buffer[k]^=buffer[len-k-1];
        buffer[len-k-1]^=buffer[k];
        buffer[k]^=buffer[len-k-1];
    }
    //null terminate the buffer and return the length in digits
    buffer[len]='\0';

    return buffer;
}

// #include "Zeeduino.h"

void setup() {}
void loop() {}

class Test
{
private:
    const char* location;
public:
    Test(const char* loc){ this->location = loc; };
    void sayHi(void){printf("Hi from %s class!!\n", this->location);}
};

Test testglobal("global");

void mainLoop( void )
{
    int test_var = 135;
    test_var += 0x10; // =151

    print_uart0("0x");
    print_uart0(toHex(test_var)); // 0x97
    print_uart0("\n");

  // delay(1);
 print_uart0("Hello world from Cortex-M3!\n");

 printf("printf hola!\n");

 Test *testlocal = new Test("dynamic");
 testlocal->sayHi();
 testglobal.sayHi();

	setup();

	while(1)
	{
		loop();
		// if (serialEventRun) serialEventRun();
	}
}
