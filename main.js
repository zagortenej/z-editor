const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
// IPC
ipcMain = electron.ipcMain

const path = require('path')
const url = require('url')
const fs = require('fs')

var ref = require('ref')
var ffi = require('ffi')

// https://stackoverflow.com/a/39604463/2268559
var user32 = new ffi.Library('user32', {
    'GetTopWindow': ['long', ['long']],
    'FindWindowA': ['long', ['string', 'string']],
    'SetActiveWindow': ['long', ['long']],
    'SetForegroundWindow': ['bool', ['long']],
    'BringWindowToTop': ['bool', ['long']],
    'ShowWindow': ['bool', ['long', 'int']],
    'SwitchToThisWindow': ['void', ['long', 'bool']],
    'GetForegroundWindow': ['long', []],
    'AttachThreadInput': ['bool', ['int', 'long', 'bool']],
    'GetWindowThreadProcessId': ['int', ['long', 'int']],
    'SetWindowPos': ['bool', ['long', 'long', 'int', 'int', 'int', 'int', 'uint']],
    'SetFocus': ['long', ['long']]
});

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1280,
    height: 1024,
    // http://electron.atom.io/docs/faq/#i-can-not-use-jqueryrequirejsmeteorangularjs-in-electron
    // webPreferences: {
    //   nodeIntegration: false
    // }
})

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })

      var mainWindowHandle = mainWindow.getNativeWindowHandle()
      mainWindowHandle.type = ref.types.uint64
      var hwnd = ref.get( mainWindowHandle, 0 )

    mainWindow.on('focus', function(){
        // console.log('ON FOCUS')

      // var mainWindowHandle = mainWindow.getNativeWindowHandle()
      // // console.log('NativeWindowHandle', mainWindowHandle)

      // // https://msdn.microsoft.com/en-us/library/windows/desktop/ms633545(v=vs.85).aspx
      // // https://msdn.microsoft.com/en-us/library/windows/desktop/aa383751(v=vs.85).aspx
      // // https://github.com/CatalystCode/windows-registry-node/blob/master/lib/types.js
      // mainWindowHandle.type = ref.types.uint64
      // var hwnd = ref.get( mainWindowHandle, 0 )
      // console.log( hwnd )

      // 1 -> HWND_BOTTOM
      // 3 -> SWP_NOMOVE | SWP_NOSIZE
      var setWindowPos1 = user32.SetWindowPos(hwnd, 1, 0, 0, 0, 0, 3)
      // https://stackoverflow.com/a/365171/2268559
      // https://superuser.com/a/531997
    })

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

require('./electron/files.js')
require('./electron/build.js')
